Tītahi Bay
----------

SCHISM 3D baroclinic model of Titahi Bay made in support of the submission by YOUR BAY YOUR SAY (https://haveyoursay.gw.govt.nz/poriruawwtp). 
More information about the modelling can be found here: https://haveyoursay.gw.govt.nz/69251/widgets/339300/documents/234315

.. image:: images/presentation.gif

SCHISM MODEL
------------

The Semi-implicit Cross-scale Hydroscience Integrated System Model (SCHISM)
was used to hindcast the hydrodynamic ocean conditions, including the offshore
islands and the Porirua Harbour. An hour-by-hour replication of the
three-dimensional flows was prepared for the year 2018, which includes the ADCP
measurements. The triangular model mesh has resolution ranging from 15 m near
the shore to 400 m in the open ocean, with 10 vertical sigma layers.

.. image:: images/mesh.png

Ocean boundary
--------------

Ocean boundaries were defined from an existing hydrodynamical hindcast covering central New Zealand. That model includes the elevation and current amplitudes and phases of the dominant tidal constituents (M2, S2, N2, K2, K1, O1, P1, Q1, M4, MS4, MN4, MM, MF and 2N2).
Residual velocities and water column properties defined the boundary condition along the margins of the continental shelf, prescribed by the global 1/12-degree reanalysis products released by the EU-funded Copernicus Project.
Atmospheric forcing was sourced from a 4 km horizontal resolution dynamical downscaling of the ERA5 atmospheric reanalysis product. The winds and currents from this hindcast have been previously validated against observations throughout the Western Central New Zealand and Taranaki. Those data have been applied and reviewed in a range of studies such as metocean design criteria for offshore oil facilities, offshore wind energy prospecting and aquaculture site
investigations.

Model validation
----------------

Detailed validation of the model has been undertaken; a process where the colocated
model predictions are directly compared with ADCP measurements over the
same period of time.

.. image:: images/validation.png

.. image:: images/validation2.png
